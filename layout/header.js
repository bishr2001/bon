/* eslint-disable @next/next/no-html-link-for-pages */
import { FiMenu } from "react-icons/fi";
import { BsXLg } from "react-icons/bs";
import { IoIosArrowDown } from "react-icons/io";
import { IoIosArrowForward } from "react-icons/io";
import { BsTranslate } from "react-icons/bs";
import { useState, useEffect } from "react";

import Link from "next/link";
const Header = () => {
  const list = [
    {
      name: "OWNERS Page",
      link: "link",
      sup: [
        { name: "Manage Timeshare", link: "manage" },
        { name: "Buy Timeshare", link: "buy" },
        { name: "Sell Timeshare", link: "sell" },
        { name: "Rent Timeshare", link: "rent" },
        { name: "MVCI Resort", link: "mvci" },
        { name: "MVCI Destinations Points", link: "mvci-point" },
        { name: "Marriott Bonvoy", link: "bonvoy" },
        { name: "Exchange with Interval International", link: "interval" },
      ],
    },
    {
      name: "Rental Vacations",
      link: "link",
      sup: [
        { name: "Marriott’s Club Son Antem", link: "link" },
        { name: "Marriott’s playa andaluza", link: "link" },
        { name: "Marriott's Village d'ile-de-France", link: "link" },
        { name: "Marriott’s marbella beach resort", link: "link" },
        {
          name: "Grand Residences by Marriott-Mayfair-London 47 park",
          link: "link",
        },
        { name: "Marriott’s newport coast villas", link: "link" },
        { name: "Marriott’s Ko Olina Beach Club", link: "link" },
        {
          name: "Marriott's Phuket Beach Resort & Marriott’s Mai Khao Beach",
          link: "link",
        },
      ],
    },
    {
      name: "Europe Resort",
      link: "link",
      sup: [
        { name: "Marriott’s Marbella Beach Resort", link: "link" },
        { name: "Marriott’s Club Son Antem", link: "link" },
        { name: "Marriott’s Playa Andaluza", link: "link" },
        { name: "Marriott's Village d'ile-de-France", link: "link" },
      ],
    },
    {
      name: "Travel & Tourism",
      link: "link",
      sup: [
        {
          name: "Hotel, Apartment & Resort Reservations",
          link: "hotel_resort",
        },
        { name: "Airline Reservations", link: "airline_reservations" },
        { name: "Excursions & Tours", link: "excursions_tours" },
        { name: "Transfer Services", link: "transfer_services" },
        { name: "MICE & Corporate Events", link: "mice_corporate" },
        { name: "Visa & Travel Insurance", link: "visa_travel" },
        { name: "Outbound Travel", link: "outbound_travel" },
      ],
    },
    {
      name: "Services",
      link: "link",
      sup: [
        { name: "Tourism and Recreation Consultants", link: "link" },
        { name: "Businessmen Administrative Services", link: "link" },
        { name: "Hospitality Services", link: "link" },
        { name: "Timeshare Management", link: "link" },
        { name: "Public Relations Management", link: "link" },
        { name: "Marketing Management", link: "link" },
      ],
    },
    {
      name: "About Us",
      link: "link",
      sup: [
        { name: "About Us", link: "about" },
        { name: "Payment Gateway", link: "payment" },
        { name: "Terms & conditions", link: "terms" },
        { name: "Privacy policy", link: "privacy" },
        { name: "Refund policy", link: "refund" },
        { name: "Membership", link: "membership" },
      ],
    },
  ];
  const [open, setOpen] = useState(false);
  const [openSup, setOpenSup] = useState("");

  const [clientWindowHeight, setClientWindowHeight] = useState("");

  const handleScroll = () => {
    setClientWindowHeight(window.scrollY);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  });

  return (
    <header
      className={` ${
        clientWindowHeight != 0 ? "lg:to-black lg:from-black" : ""
      } bg-black lg:bg-transparent w-full  fixed lg:bg-gradient-to-b   lg:from-[#00000088]  lg:px-32 z-10`}
    >
      <div className="flex justify-between  p-4">
        <img
          className="lg:hidden"
          src={"/images/icons/logo.png"}
          alt="Picture of the author"
          width={200}
          height={40}
        />
        <img
          className={`hidden lg:block ${
            clientWindowHeight != 0 ? "" : "brightness-200"
          }`}
          src={"images/icons/logo.svg"}
          alt="Picture of the author"
          width={100}
          height={70}
        />
        <ui className=" list-none text-white lg:flex mx-8  hidden">
          {list.map((el) => {
            return (
              <li
                key={el.name}
                className=" hover:text-main cursor-pointer relative  group"
              >
                <div className="flex justify-between p-4   text-base ">
                  <div style={{ textShadow: "#000000b0 0 0 10px" }}>
                    {el.name}
                  </div>
                  <a
                    className="pt-1"
                    onClick={() => {
                      if (openSup == el.name) {
                        setOpenSup("");
                      } else {
                        setOpenSup(el.name);
                      }
                    }}
                  ></a>
                </div>
                <ul
                  className={`w-72 rounded-md absolute   text-black  text-base  group-hover:block  hidden`}
                >
                  {el.sup.map((sup, index) => {
                    return (
                      <li
                        key={index}
                        className="p-2  border-stone-600 border-b-[1px] px-4 hover:bg-main w-72 bg-white first:rounded-t-sm last:rounded-b-sm shadow-md"
                      >
                        <Link href={sup.link}>
                          <div>{sup.name}</div>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </li>
            );
          })}
        </ui>
        <div className="p-4 hidden lg:block">
          <BsTranslate
            className="text-white  text-lg hover:text-main cursor-pointer "
            style={{ textShadow: "#000 0 0 10px" }}
          />
        </div>
        <a
          className=" lg:hidden"
          onClick={() => {
            setOpen(!open);
          }}
        >
          {!open ? (
            <FiMenu className=" text-white  text-4xl" />
          ) : (
            <BsXLg className=" text-white  text-4xl" />
          )}
        </a>
      </div>
      <div className={`bg-main ${open ? "block" : "hidden"}`}>
        <ui className=" list-none text-white  ">
          {list.map((el) => {
            return (
              <li
                key={el.name}
                className={` ${open ? "block" : "hidden"} cursor-pointer`}
              >
                <div className="flex justify-between p-4  text-lg">
                  <div>{el.name}</div>
                  <a
                    className="pt-1"
                    onClick={() => {
                      if (openSup == el.name) {
                        setOpenSup("");
                      } else {
                        setOpenSup(el.name);
                      }
                    }}
                  >
                    {openSup == el.name ? (
                      <IoIosArrowDown />
                    ) : (
                      <IoIosArrowForward />
                    )}
                  </a>
                </div>
                <ul
                  className={`w-full bg-white  text-black  text-base ${
                    openSup == el.name ? "block" : "hidden"
                  }`}
                >
                  {el.sup.map((sup, index) => {
                    return (
                      <li
                        key={index}
                        className="p-2  border-stone-600 border-b-[1px] px-4"
                      >
                        <Link href={sup.link}>
                          <div>{sup.name}</div>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </li>
            );
          })}
        </ui>
        <div className="px-4 pb-4">
          <BsTranslate className="text-white  text-lg" />
        </div>
      </div>
    </header>

    // <header className="header-v3">
    //   <nav className="container-header-desktop ">
    //     <div className="wrap-menu-desktop bg-transparent">
    //       <div className="limiter-menu-desktop">
    //         <div className="menu-desktop">
    //           <a className="logo-v2 h-0" href="/">
    //             <img
    //               className=" w-24"
    //               src="/images/icons/logo.svg"
    //               alt="LOGO"
    //             />
    //           </a>

    //           <ul className="main-menu respon-sub-menu header-menu">
    //

    //             <li>
    //               <a href="#">{"About Us"}</a>
    //               <ul className="sub-menu">
    //                 <li>
    //                   <a href="/about">
    //                     <strong>{"About Us"}</strong>
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <a href="/payment">
    //                     <strong>{"Payment Gateway"}</strong>
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <a href="/terms">
    //                     <strong>{"Terms & conditions"}</strong>
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <a href="/privacy">
    //                     <strong>{"Privacy policy"}</strong>
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <a href="/refund">
    //                     <strong>{"Refund policy"}</strong>
    //                   </a>
    //                 </li>
    //                 <li>
    //                   <a href="/membership">
    //                     <strong>{"Membership"}</strong>
    //                   </a>
    //                 </li>
    //               </ul>
    //             </li>
    //           </ul>
    //           <ul className="float-end main-menu respon-sub-menu">
    //             <li className="">
    //               <a
    //                 href="/lang/en"
    //                 className="inline-block py-2 px-4 no-underline smoothlink"
    //               >
    //                 <i className="fa fa-language"></i>
    //               </a>
    //             </li>
    //             <li className="">
    //               <a
    //                 href="/lang/ar"
    //                 className="inline-block py-2 px-4 no-underline smoothlink"
    //               >
    //                 <i className="fa fa-language"></i>
    //               </a>
    //             </li>
    //           </ul>
    //         </div>
    //       </div>
    //     </div>
    //   </nav>

    //   <nav className="container-header-mobile">
    //     <div className="wrap-header-mobile">
    //       <div className="logo-mobile">
    //         <a href="/">
    //           <img src="/images/icons/logo-01.png" alt="LOGO" />
    //         </a>
    //       </div>

    //       <div className="btn-show-menu-mobile hamburger hamburger--squeeze">
    //         <span className="hamburger-box">
    //           <span className="hamburger-inner"></span>
    //         </span>
    //       </div>
    //     </div>

    //     <div className="menu-mobile">
    //       <ul className="main-menu">
    //         <li>
    //           <a href="#">{"OWNERS Page"}</a>
    //           <ul className="sub-menu">
    //             <li>
    //               <a href="/manage">
    //                 <strong>{"Manage Timeshare"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/buy">
    //                 <strong>{"Buy Timeshare"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/sell">
    //                 <strong>{"Sell Timeshare"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/rent">
    //                 <strong>{"Rent Timeshare"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/mvci">
    //                 <strong>{"MVCI Resort"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/mvci-point">
    //                 <strong>{"MVCI Destinations Points"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/bonvoy">
    //                 <strong>{"Marriott Bonvoy"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/interval">
    //                 <strong>{"Exchange with Interval International"}</strong>
    //               </a>
    //             </li>
    //           </ul>
    //         </li>
    //         <li>
    //           <a href="#clubs">{"Rental Vacations"}</a>
    //           <ul className="sub-menu">
    //             <li>
    //               <a href="/clubs/1">
    //                 <strong>{"Marriott’s Club Son Antem"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/clubs/2">
    //                 <strong>{"Marriott’s playa andaluza"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/clubs/3">
    //                 <strong>{"Marriott's Village d'ile-de-France"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/clubs/6">
    //                 <strong>{"Marriott’s marbella beach resort"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/clubs/5">
    //                 <strong>
    //                   {"Grand Residences by Marriott-Mayfair-London 47 park"}
    //                 </strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/clubs/4">
    //                 <strong>{"Marriott’s newport coast villas"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/clubs/42">
    //                 <strong>{"Marriott’s Ko Olina Beach Club"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/clubs/80">
    //                 <strong>
    //                   {
    //                     "Marriott's Phuket Beach Resort & Marriott’s Mai Khao Beach"
    //                   }
    //                 </strong>
    //               </a>
    //             </li>
    //           </ul>
    //         </li>
    //         <li>
    //           <a href="#travel-n-tourism">{"Europe Resort"}</a>
    //           <ul className="sub-menu">
    //             <li>
    //               <a href="/rentals/2">
    //                 <strong>{"Marriott’s Marbella Beach Resort"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/rentals/1 ">
    //                 <strong>{"Marriott’s Club Son Antem"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/rentals/3">
    //                 <strong>{"Marriott’s Playa Andaluza"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/rentals/4 ">
    //                 <strong>{"Marriott's Village d'ile-de-France"}</strong>
    //               </a>
    //             </li>
    //           </ul>
    //         </li>

    //         <li>
    //           <a href="#travel-n-tourism">{"Travel & Tourism"}</a>
    //           <ul className="sub-menu">
    //             <li>
    //               <a href="/travel-n-tourism/4">
    //                 <strong>{"Hotel, Apartment & Resort Reservations"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/travel-n-tourism/4">
    //                 <strong>{"Airline Reservations"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/travel-n-tourism/4">
    //                 <strong>{"Excursions & Tours"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/travel-n-tourism/4">
    //                 <strong>{"Transfer Services"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/travel-n-tourism/4">
    //                 <strong>{"MICE & Corporate Events"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/travel-n-tourism/4">
    //                 <strong>{"Visa & Travel Insurance"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/travel-n-tourism/4">
    //                 <strong>{"Outbound Travel"}</strong>
    //               </a>
    //             </li>
    //           </ul>
    //         </li>

    //         <li>
    //           <a href="/#service">{"Services"}</a>
    //           <ul className="sub-menu">
    //             <li>
    //               <a href="/services/4">
    //                 <strong>{"Tourism and Recreation Consultants"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/services/5">
    //                 <strong>{"Businessmen Administrative Services"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/services/6">
    //                 <strong>{"Hospitality Services"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/services/9">
    //                 <strong>{"Timeshare Management"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/services/3">
    //                 <strong>{"Public Relations Management"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/services/1">
    //                 <strong>{"Marketing Management"}</strong>
    //               </a>
    //             </li>
    //           </ul>
    //         </li>
    //         <li>
    //           <a href="#">{"About Us"}</a>
    //           <ul className="sub-menu">
    //             <li>
    //               <a href="/about">
    //                 <strong>{"About Us"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/payment">
    //                 <strong>{"Payment Gateway"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/terms">
    //                 <strong>{"Terms & conditions"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/privacy">
    //                 <strong>{"Privacy policy"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/refund">
    //                 <strong>{"Refund policy"}</strong>
    //               </a>
    //             </li>
    //             <li>
    //               <a href="/membership">
    //                 <strong>{"Membership"}</strong>
    //               </a>
    //             </li>
    //           </ul>
    //         </li>

    //         <li className="">
    //           <a
    //             href="/lang/en"
    //             className="inline-block py-2 px-4 no-underline smoothlink"
    //           >
    //             <i className="fa fa-language"></i>
    //           </a>
    //         </li>
    //         <li className="">
    //           <a
    //             href="/lang/ar"
    //             className="inline-block py-2 px-4 no-underline smoothlink"
    //           >
    //             <i className="fa fa-language"></i>
    //           </a>
    //         </li>
    //       </ul>
    //     </div>
    //   </nav>
    // </header>
  );
};
export default Header;
