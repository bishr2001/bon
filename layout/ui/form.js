import data from "../../staticData/countries.json";

export default function MainForm({ header, body }) {
  const countries = data;

  return (
    <div className="px-8 py-16 lg:w-1/2 m-auto">
      <div>
        <img className="m-auto" src="/images/contact.gif" width={150} />
      </div>
      <div className=" text-center pb-4">
        <div className=" text-2xl font-bold text-[#333]">
          {header ? header : "Send Us A Message"}
        </div>
        <div className=" text-sm text-[#666]">
          {body
            ? body
            : "Our team loves questions and feedback. Here are some ways to contact us"}
        </div>
      </div>

      <div>
        <form id="contact-form-main" className="validate-form">
          <div className="">
            <input
              className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
              type="text"
              name="name"
              placeholder="Your Name"
              required
            />
          </div>
          <div className="">
            <input
              className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
              type="email"
              name="email"
              placeholder="your Email"
              required
            />
          </div>

          <div className=" validate-input" data-validate="Phone is required">
            <div className="flex flex-wrap  ">
              <div className=" lg:w-1/2 w-full lg:pr-2">
                <select
                  className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
                  name="phonecode"
                  required
                >
                  <option value="" disabled selected>
                    Select Country
                  </option>
                  {countries.map((val) => {
                    return (
                      <option key={val.id} className="" value={val.dial}>
                        {val.name} (+{val.dial})
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="lg:w-1/2 w-full lg:pl-2">
                <input
                  className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
                  type="number"
                  name="phone"
                  placeholder="Phone Number"
                  required
                />
              </div>
            </div>
          </div>

          <div className=" mb-3">
            <div className="w-full">
              <select
                className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
                name="subject"
              >
                <option value="">{"Select Subject"}</option>
                <option value="Rent">{"Rent"}</option>
                <option value="Buy">{"Buy"}</option>
                <option value="Sell">{"Sell"}</option>
                <option value="Inquiry">{"Inquiry"}</option>
                <option value="Manage">{"Manage"}</option>
                <option value="Other">{"Other"}</option>
              </select>
            </div>
          </div>

          <div
            className="m-b-30 validate-input"
            data-validate="Message is required"
          >
            <textarea
              className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
              name="content"
              placeholder="Your Message"
              required
            ></textarea>
          </div>

          <div className="mb-4 validate-input  text-base">
            <input
              type="checkbox"
              id="checkbox-policy"
              className=" w-4 h-4 inline mr-4"
              required
              data-validate="Checkbox policy is required"
            />
            <label for="checkbox-policy" className=" inline text-sm font-bold">
              {
                "I consent to bon voyage to collect my data via this form.(Required)"
              }
            </label>
            <h5 className="pt-2 text-sm font-bold">
              {"Please Check our"}{" "}
              <a href="/privacy" className=" text-blue-500">
                {"Privacy Policy"}
              </a>{" "}
              {"to see how we protect and manage your submitted data."}
            </h5>
          </div>

          <button
            type="submit"
            id="submit-main"
            className=" text-white p-3 px-5  bg-main"
          >
            {"Send Email"}
          </button>
        </form>
        <div className="h-[1px] bg-main my-4" />
        <div className="flex justify-end">
          <button className=" text-white p-3 px-5  bg-main">
            {"Make Payment"}
          </button>
        </div>
      </div>
    </div>
  );
}
