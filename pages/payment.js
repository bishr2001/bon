export default function Home() {
  return (
    <div className="container mx-auto">
      <Payment />
    </div>
  );
}

const Payment = () => {
  return (
    <div className="px-8 py-16  pt-28  lg:w-2/5 m-auto">
      <h1 className=" font-semibold text-3xl pb-8">Payment Form</h1>
      <div>
        <form id="contact-form-main" className="validate-form">
          <input
            className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
            type="text"
            name="name"
            placeholder=" Name"
            required
          />
          <input
            className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
            type="text"
            name="country"
            placeholder=" Country"
            required
          />
          <input
            className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
            type="text"
            name="state"
            placeholder="State"
            required
          />
          <input
            className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
            type="text"
            name="city"
            placeholder="City"
            required
          />
          <input
            className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
            type="text"
            name="zip"
            placeholder="ZIP"
            required
          />
          <input
            className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
            type="text"
            name="address"
            placeholder="Address"
            required
          />
          <input
            className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
            type="email"
            name="email"
            placeholder="your Email"
            required
          />
          <input
            className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
            type="text"
            name="phone"
            placeholder="Phone Number"
            required
          />
          <input
            className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
            type="text"
            name="amount"
            placeholder="Amount"
            required
          />
          <select
            className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
            name="curancy"
          >
            <option value="AED" selected>
              {"AED"}
            </option>
            <option value="EUR">{"EUR"}</option>
            <option value="USD">{"USD"}</option>
          </select>

          <div
            className="m-b-30 validate-input"
            data-validate="Message is required"
          >
            <textarea
              className="w-full p-2 rounded-md my-2 px-4 border-[#ccc] border-[1px]  focus-visible:border-main focus-visible:border-[1px]"
              name="content"
              placeholder="Remarks"
              required
            ></textarea>
          </div>

          <button
            type="submit"
            id="submit-main"
            className=" text-white p-3 px-5  bg-main"
          >
            {"Submit Order"}
          </button>
        </form>
      </div>
    </div>
  );
};
