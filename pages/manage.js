import MainForm from "@/layout/ui/form";
import Link from "next/link";
import Marquee from "react-fast-marquee";

export default function Home() {
  return (
    <div className="container mx-auto ">
      <Hotel />
      <MainForm
        header={"Why choose us?"}
        body={
          "With over 15 years of experience managing timeshare, we can help We manage your account from A-Z process hassle free either buying, selling renting and many more."
        }
      />
    </div>
  );
}

const Hotel = () => {
  const clubs = [
    {
      name: "Marriott Club Son Antem",
      link: "clups/1",
      image: "/images/project-01.webp",
    },
    {
      name: "Marriott’s Playa Andaluza",
      link: "clups/2",
      image: "/images/project-02.webp",
    },
    {
      name: "Marriott's Village d'ile-de-France",
      link: "clups/3",
      image: "/images/project-03.webp",
    },

    {
      name: "Grand Residences by Marriott - Mayfair - London 47 Park Street",
      link: "clups/5",
      image: "/images/project-05.webp",
    },
    {
      name: "Marriott’s Marbella Beach Resort",
      link: "clups/6",
      image: "/images/project-06.webp",
    },

    {
      name: "Marriott's Phuket Beach Resort & Marriott’s Mai Khao Beach",
      link: "clups/80",
      image: "/images/80-logo.webp",
    },
  ];
  return (
    <div className="pt-[100px] ">
      <div className="flex flex-wrap ">
        <div className=" p-4 w-full lg:w-1/3">
          <img
            src="/images/owners/1.png"
            className="w-[300px] h[600px] object-cover object-center m-auto"
          />
        </div>

        <div className=" w-full lg:w-2/3 px-6 lg:pt-4">
          <h2 className=" text-4xl  text-[#333] pb-4">Manage Timeshare</h2>
          <p className="pt-3 text-[#666]    font-extralight">
            Timeshare is a relatively recent business format supporting
            hospitality and tourism services. Simply said, timeshare formats
            allow customers to access the right to accommodation within and
            between countries. The timeshare owner buys accommodation of a
            certain quality for a given period. There are several different
            schemes, but the most simple and original format involves the
            timeshare owner buying the right to stay self-catering apartment or
            townhouse/villa in the same resort for a fixed week each year.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Timeshare is a fantastic travel solution but sometimes it’s hard.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            And we are here for you! We fully assist you in managing your
            timeshare account, booking your personal vacations, exchanging, or
            renting of your owned week (s), buying, selling, and doing rentals.
            Assigned with skilled professionals with unlimited working
            experience in travel and tourism, hospitality services and timeshare
            for more than 15 years in Marriott Properties. We have the knowledge
            about the system, how everything works, and we can help owners the
            best way we can. We specialize in renting the owner weeks, resales,
            offering them new weeks to purchase, changing the season, the
            resort, unit’s size, modifying the owner’s name in the event of
            death or other family situation and all the administrative aspects
            related to the changes. Our expertise comes very handy if you are
            looking for a vacation account manager or your personal secretary in
            all your travel needs worldwide.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            We aren’t a listing site – which means you don’t book reservations
            and list them with us. We are a full-service management agency,
            which means we will help with all aspects of your ownership
            including renting any points you are not using. You simply tell us
            how many points you want to rent, and we do all the work from there.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Due to the marketing reach we have through the online travel
            agencies; we don’t need to rely on the pre-booking model that many
            smaller agencies do. Instead, we have the luxury of being able to
            answer incoming requests from thousands of guests per month, picking
            up exact reservations for the guest instead of guessing at what a
            potential guest will want to book.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            We are one of the largest third-party management and rental agency
            in the nation, helping point-based timeshare owners and guests since
            20. We partner with the largest online travel agencies in the world
            and get exposure to over 100 million renters and travelers worldwide
            in a month, making it easy to get your excess points rented out.
          </p>
          <p className="pt-24 text-[#666]  font-bold text-center">
            Our focused timeshare is with Marriott Properties Worldwide
            <Link href={"mvci"} className=" text-blue-500">
              Bonvoyage (bon-voyage.services)
            </Link>
            specializing SPAIN, FRANCE, UK & THAILAND
          </p>
        </div>
        <div className="flex flex-wrap  justify-center">
          {clubs.map((val) => {
            return (
              <ClubCard
                key={val.name}
                name={val.name}
                link={val.link}
                image={val.image}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

const ClubCard = ({ image, name, link, travel }) => {
  return (
    <Link href={link} className={` lg:w-1/3  w-full p-2`}>
      <div className="flex justify-center pb-8 cursor-pointer w-full ">
        <div>
          <img
            src={image}
            className=" h-[220px] w-[270px] object-cover  hover:scale-105  duration-300 ease-in"
          />
          <div className="py-4 text-center text-[#886c3c] font-bold hover:text-main  text-lg max-w-[270px]">
            {name}
          </div>
          <p className=" text-[#666]   text-center  font-extralight">
            Available (Buy , Sell , Rent)
          </p>
        </div>
      </div>
    </Link>
  );
};
