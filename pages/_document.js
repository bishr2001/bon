import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en">
      <Head />
      <body className="  w-fit h-screen">
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
