import MainForm from "@/layout/ui/form";
import Link from "next/link";
import Marquee from "react-fast-marquee";

export default function Home() {
  return (
    <div className="container mx-auto ">
      <Hotel />
      <MainForm
        header={
          "Searching for the easiest, safest, and quickest way to rent timeshare and book resort vacations for up to 50% off."
        }
        body={
          "With a timeshare rental, you can go vacation in a more comfortable, luxurious way, all for less than you would pay at a nearby hotel as it is direct from owners."
        }
      />
    </div>
  );
}

const Hotel = () => {
  const clubs = [
    {
      name: "Marriott Club Son Antem",
      link: "clups/1",
      image: "/images/project-01.webp",
    },
    {
      name: "Marriott’s Playa Andaluza",
      link: "clups/2",
      image: "/images/project-02.webp",
    },
    {
      name: "Marriott's Village d'ile-de-France",
      link: "clups/3",
      image: "/images/project-03.webp",
    },

    {
      name: "Grand Residences by Marriott - Mayfair - London 47 Park Street",
      link: "clups/5",
      image: "/images/project-05.webp",
    },
    {
      name: "Marriott’s Marbella Beach Resort",
      link: "clups/6",
      image: "/images/project-06.webp",
    },

    {
      name: "Marriott's Phuket Beach Resort & Marriott’s Mai Khao Beach",
      link: "clups/80",
      image: "/images/80-logo.webp",
    },
  ];
  return (
    <div className="pt-[100px] ">
      <div className="flex flex-wrap ">
        <div className=" p-4 w-full lg:w-1/3">
          <img
            src="/images/owners/1.png"
            className="w-[300px] h[600px] object-cover object-center m-auto"
          />
        </div>

        <div className=" w-full lg:w-2/3 px-6 lg:pt-4">
          <h2 className=" text-4xl  text-[#333] pb-4">Rent Timeshare</h2>
          <p className="pt-3 text-[#666]    font-extralight">
            Marriott Timeshares typically become available for rent when the
            owner does not need the unit during a specific period. Renting a
            timeshare is a good way to try one out before you purchase and/or to
            give yourself another option when planning a vacation. There is an
            undeniably large demand for renting timeshare. Whether it is owners
            looking to get the most out of their unwanted timeshare before they
            sell it on, owners wanting to rent extra weeks each year without
            having maintenance fees to pay every single year, or simply those
            looking to try before they buy. Bonus weeks can sometimes become
            available to owners, but they are often charged at a high premium or
            get snapped up too quickly to be relied upon. The timeshare resales
            that Bon Voyage Services has available are always offered with
            savings of up to 35% off the original purchase price. Our inventory
            listing changes from time to time which ensures fresh new resale
            ownerships for you to search for. We establish the ownership and
            availability of the property; we issue legal rental contracts to
            both. Should the rental not be available, we will try to find
            alternative stock.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Renting timeshare is a fantastic solution for those looking to
            recoup the cost of their maintenance fees if they have been unable
            to use their timeshare, or for those looking for a no-risk way to
            experience the timeshare resort at which they are looking to
            purchase a week or two. It means that owners can receive a rental
            income, and potential owners get a real inside look at their desired
            home resort rather than relying solely on advertising and reviews.
          </p>
          <p className="pt-3 text-[#666]  text-lg   font-extralight">
            HOW IT WORKS
          </p>
          <p className="pt-3 text-[#666]    font-bold">
            STEP 1: YOU APPOINT/HIRE US
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            We have a simple agreement that you would fill out and sign, which
            authorizes us to make, modify and/or cancel reservations on your
            behalf.
          </p>
          <p className="pt-3 text-[#666]    font-bold">
            STEP 2: WE ADVERTISE DATES FOR WEEKLY RENTAL
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            We are listed on all the largest online travel agencies in the world
            making it easy for renters to find us. We receive thousands of
            inquiries every single month.
          </p>
          <p className="pt-3 text-[#666]    font-bold">
            STEP 3: WE BOOK RESERVATIONS
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Once a client confirmed a townhouse/apartment, we pick up the
            reservation in your account.
          </p>
          <p className="pt-3 text-[#666]    font-bold">
            STEP 4: WE MANAGE TRANSACTIONS
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            We will handle every aspect of the rental transaction from start to
            finish or A-Z. Everything from collecting payment to getting the
            guest checked in.
          </p>
          <p className="pt-3 text-[#666]    font-bold">STEP 5: WE PAY YOU</p>
          <p className="pt-3 text-[#666]    font-extralight">
            You will never pay us any money. From the rental proceeds, we shall
            get only our commission and send the rest to your bank account.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            It is important when you are searching for a timeshare resale to
            ensure you know the main products available on the market. We have
            set these out into five main categories:
          </p>

          <p className="pt-24 text-[#666]  font-bold text-center">
            Our focused timeshare is with Marriott Properties Worldwide
            <Link href={"mvci"} className=" text-blue-500">
              Bonvoyage (bon-voyage.services)
            </Link>
            specializing SPAIN, FRANCE, UK & THAILAND
          </p>
        </div>
        <div className="flex flex-wrap pt-8  justify-center">
          {clubs.map((val) => {
            return (
              <ClubCard
                key={val.name}
                name={val.name}
                link={val.link}
                image={val.image}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

const ClubCard = ({ image, name, link, travel }) => {
  return (
    <Link href={link} className={` lg:w-1/3  w-full p-2`}>
      <div className="flex justify-center pb-8 cursor-pointer w-full ">
        <div>
          <img
            src={image}
            className=" h-[220px] w-[270px] object-cover  hover:scale-105  duration-300 ease-in"
          />
          <div className="py-4 text-center text-[#886c3c] font-bold hover:text-main  text-lg max-w-[270px]">
            {name}
          </div>
          <p className=" text-[#666]   text-center  font-extralight">
            Available (Buy , Sell , Rent)
          </p>
        </div>
      </div>
    </Link>
  );
};
