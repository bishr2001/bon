import MainForm from "@/layout/ui/form";
import Link from "next/link";
import Marquee from "react-fast-marquee";

export default function Home() {
  return (
    <div className="container mx-auto ">
      <Hotel />
      <MainForm
        header={"Need to sell timeshare?"}
        body={
          "There are buyers on our site right now searching for timeshares just like yours. Get answers here instantly and access to best marketing platforms and strong database of Buyers"
        }
      />
    </div>
  );
}

const Hotel = () => {
  const clubs = [
    {
      name: "Marriott Club Son Antem",
      link: "clups/1",
      image: "/images/project-01.webp",
    },
    {
      name: "Marriott’s Playa Andaluza",
      link: "clups/2",
      image: "/images/project-02.webp",
    },
    {
      name: "Marriott's Village d'ile-de-France",
      link: "clups/3",
      image: "/images/project-03.webp",
    },

    {
      name: "Grand Residences by Marriott - Mayfair - London 47 Park Street",
      link: "clups/5",
      image: "/images/project-05.webp",
    },
    {
      name: "Marriott’s Marbella Beach Resort",
      link: "clups/6",
      image: "/images/project-06.webp",
    },

    {
      name: "Marriott's Phuket Beach Resort & Marriott’s Mai Khao Beach",
      link: "clups/80",
      image: "/images/80-logo.webp",
    },
  ];
  return (
    <div className="pt-[100px] ">
      <div className="flex flex-wrap ">
        <div className=" p-4 w-full lg:w-1/3">
          <img
            src="/images/owners/1.png"
            className="w-[300px] h[600px] object-cover object-center m-auto"
          />
        </div>

        <div className=" w-full lg:w-2/3 px-6 lg:pt-4">
          <h2 className=" text-4xl  text-[#333] pb-4">Sell Timeshare</h2>
          <p className="pt-3 text-[#666]    font-bold">
            Free Registration & Tailor-Made Adverts
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Owning a timeshare can be an amazing experience but life changes and
            so do your needs. You may find you no longer have the time to use
            it. Or you may be paying maintenance fees for a product that doesn’t
            fit your travel habits.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Whatever the reason, if you’re looking for a way out, Bon Voyage
            Services can guide you through your options.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Selling a Timeshare Ownership with Bon Voyage Services offer a
            complete timeshare resale service which allows you to sell your
            timeshare with confidence as it’s simple and effective. We will give
            you a realistic idea of the price you can get on the resale market.
            Marriott does not buy back your week anyway; they will suggest
            surrendering the week. We suggest trying to get the best of it. We
            will advertise the week to finding the buyer and we will take care
            of transfer the week. When we receive a purchase offer for your
            timeshare, we will confirm to you in writing the exact net amount
            which you will receive upon completion of the transfer. You then
            choose if you want to accept the offer. For your security, the funds
            for the transfer will always be held in a protect account, till the
            time we receive the Owner Certificate from Marriott Vacation Club.
            In case of a sale company is intitle to receive 5% + TVA 5% as
            commission. This amount will be deducted from the sale price. For
            your security, the funds for the transfer will always be held in a
            protect account, till the time we receive the Owner Certificate from
            Marriott Vacation Club.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Our service to sell your timeshare will include:
          </p>

          <ol className="pt-1 text-[#666]    font-extralight list-[upper-alpha] px-8">
            <li>Free Registration – No upfront charges</li>
            <li>
              No commission for you to pay, in all cases the buyer pays the
              commission.
            </li>
            <li>No transfer of ownership fees to pay except property fees.</li>
            <li>
              The price we offer you is the price you will receive, with no
              deductions.
            </li>
            <li>
              Advertising and marketing service which offers unrivalled exposure
              of your timeshare ownership.
            </li>
            <li>Free Valuation and Market Appraisal.</li>
          </ol>
          <p className="pt-3 text-[#666]    font-extralight">
            When deciding to sell a timeshare we ensure your ownership is placed
            in front of as many potential buyers as possible. We offer an
            unrivalled timeshare resale solution to help and assist with the
            selling of your timeshare ownership.
          </p>

          <p className="pt-24 text-[#666]  font-bold text-center">
            Our focused timeshare is with Marriott Properties Worldwide
            <Link href={"mvci"} className=" text-blue-500">
              Bonvoyage (bon-voyage.services)
            </Link>
            specializing SPAIN, FRANCE, UK & THAILAND
          </p>
        </div>
        <div className="flex flex-wrap pt-8  justify-center">
          {clubs.map((val) => {
            return (
              <ClubCard
                key={val.name}
                name={val.name}
                link={val.link}
                image={val.image}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

const ClubCard = ({ image, name, link, travel }) => {
  return (
    <Link href={link} className={` lg:w-1/3  w-full p-2`}>
      <div className="flex justify-center pb-8 cursor-pointer w-full ">
        <div>
          <img
            src={image}
            className=" h-[220px] w-[270px] object-cover  hover:scale-105  duration-300 ease-in"
          />
          <div className="py-4 text-center text-[#886c3c] font-bold hover:text-main  text-lg max-w-[270px]">
            {name}
          </div>
          <p className=" text-[#666]   text-center  font-extralight">
            Available (Buy , Sell , Rent)
          </p>
        </div>
      </div>
    </Link>
  );
};
