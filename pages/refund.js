export default function Home() {
  return (
    <div className="container mx-auto lg:px-32">
      <Hotel />
    </div>
  );
}

const Hotel = () => {
  return (
    <div className="pt-[100px]  pb-16 ">
      <div className="flex flex-wrap ">
        <div className=" p-4 w-full lg:w-1/3">
          <img
            src="/images/refund.jpg"
            className="w-full h[300px] object-cover object-center"
          />
        </div>

        <div className=" w-full lg:w-2/3 px-6 lg:pt-4">
          <h2 className=" text-4xl  text-[#333] pb-4">
            Cancellation & Refund Policy
          </h2>

          <p className="my-4 text-[#666] font-bold ">
            Marriott Timeshare Weekly Rentals (7 nights and 8 days) OR Vacation
            Rentals(7 nights and 8 days)
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            <strong className=" font-bold">Bon Voyage Services</strong> is
            acting solely as an independent broker putting together the owner
            holding the reservation and the guest who will be using the
            reservation. Bon Voyage Services do not represent Marriott Vacation
            Club. Bon Voyage Services is not responsible for adherence to the
            Marriott Vacation Club terms and conditions and is not liable in any
            way if any such terms and conditions broken. The owner of the
            week(s) and holding the above reservation is paid immediately after
            the funds are cleared by Bon Voyage Services, therefore this is a
            non-refundable, no cancellation transaction. Whatever the case, in
            the event that the renter, is not able to make use of the
            reservation, there will be no refund including a force majeure
            event.
          </p>

          <p className="my-4 text-[#666] font-bold ">Short Stays</p>

          <p className="pt-3 text-[#666]    font-extralight">
            Change of plans? Not to worry. We shall assist you as long as you
            notify us whether you ll get money back depends on the type of
            booking you made and how close you are to check-in. Heres how it
            breaks down:
          </p>
          <p className=" text-[#666]    font-extralight">
            <strong className=" font-bold">Refundable booking</strong> - If you
            arent too close to check-in hotels have their own rules about that,
            so be sure to check the fine print in your itinerary.
          </p>
          <p className=" text-[#666]    font-extralight">
            <strong className=" font-bold">Non-refundable booking?</strong> - If
            you Like the name suggests, you wont get any money back.
          </p>
          <p className="pt-3 text-[#666] font-bold ">
            How to cancel your hotel/vacation rental
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Get in touch with us and we will take care of it for you. Just keep
            in mind that the hotel may charge a fee depends on the reservation
            made.
          </p>
          <p className="my-4 text-[#666] font-bold ">
            Other Information’s you need to know
          </p>

          <p className="pt-3 text-[#666]    font-extralight">
            <strong className=" font-bold">NO SHOWS</strong> - If you dont show
            up, or show up outside of check-in hours, the hotel may charge you.
            The Rules and Restrictions section in your itinerary has all the
            fine print.
          </p>
          <p className=" text-[#666]    font-bold">Book a vacation package?</p>
          <p className=" text-[#666]    font-bold">
            Just need to change something? Try changing your booking instead of
            canceling it. Book a vacation package?
          </p>
          <p className=" text-[#666]    font-bold">
            Still need help? Get in touch. We are here for you.
          </p>
        </div>
      </div>
    </div>
  );
};
