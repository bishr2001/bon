import MainForm from "@/layout/ui/form";
import Link from "next/link";
import Marquee from "react-fast-marquee";

export default function Home() {
  return (
    <div className="container mx-auto ">
      <Hotel />
      <MainForm
        header={"Looking for the right Timeshare?"}
        body={
          "Get answers here instantly. We have a vast selection of timeshare weeks and timeshare points for sale typically discounted 50-90% from their original prices."
        }
      />
    </div>
  );
}

const Hotel = () => {
  const clubs = [
    {
      name: "Marriott Club Son Antem",
      link: "clups/1",
      image: "/images/project-01.webp",
    },
    {
      name: "Marriott’s Playa Andaluza",
      link: "clups/2",
      image: "/images/project-02.webp",
    },
    {
      name: "Marriott's Village d'ile-de-France",
      link: "clups/3",
      image: "/images/project-03.webp",
    },

    {
      name: "Grand Residences by Marriott - Mayfair - London 47 Park Street",
      link: "clups/5",
      image: "/images/project-05.webp",
    },
    {
      name: "Marriott’s Marbella Beach Resort",
      link: "clups/6",
      image: "/images/project-06.webp",
    },

    {
      name: "Marriott's Phuket Beach Resort & Marriott’s Mai Khao Beach",
      link: "clups/80",
      image: "/images/80-logo.webp",
    },
  ];
  return (
    <div className="pt-[100px] ">
      <div className="flex flex-wrap ">
        <div className=" p-4 w-full lg:w-1/3">
          <img
            src="/images/owners/1.png"
            className="w-[300px] h[600px] object-cover object-center m-auto"
          />
        </div>

        <div className=" w-full lg:w-2/3 px-6 lg:pt-4">
          <h2 className=" text-4xl  text-[#333] pb-4">Buy Timeshare</h2>
          <p className="pt-3 text-[#666]    font-bold">
            (Guaranteed 75% Off Retail & A Holiday for A Lifetime)
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            A major strength of buying timeshare is the certainty that it
            provides in popular holiday locations. The high standard
            accommodation in luxurious apartments with superb facilities and
            amazing value for money only some of the many benefits.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            A lifetime of quality holidays for you and your family will be
            secured by purchasing a quality timeshare Accommodation being held
            in trust for the lifetime of the right to use. When buying
            timeshare, you pay a once off payment for the right to use. Each
            year, there is a maintenance fee which pays for the accommodation
            etc. to be kept clean, in good order, local taxes paid, insurance
            etc.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            The advantages of buying a timeshare resale through Bon Voyage
            Services are that you buy privately from existing owners offering
            realistic process over 75% on the developers list price. You will
            buy from private owners the right to use at Marriott resort of your
            choice for one week or more per year. We will take care of every
            step needed to bring the Marriott ownership in your name, we will
            communicate with Marriott Owner modification service. We will assist
            you till the moment you will receive the certificate as a proof of
            ownership once the purchase transaction is completed and open for
            you the relevant account in MVCI, Bonvoy, Interval International.
            And more importantly we will help you to manage it and you will save
            a lot of money.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Buying a timeshare through Bon Voyage Services allows you all the
            same benefits to exchange through Interval International into over
            3000 timeshare resorts. The only difference you will notice is the
            significantly lower upfront price you pay for the timeshare. We are
            highly recommended reseller of Marriott Properties.
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            Different Types of Timeshares
          </p>
          <p className="pt-3 text-[#666]    font-extralight">
            It is important when you are searching for a timeshare resale to
            ensure you know the main products available on the market. We have
            set these out into five main categories:
          </p>

          <ol className="pt-1 text-[#666]    font-extralight list-[upper-alpha] px-8">
            <li>
              Fixed timeshare ownership - This is a fixed week number which you
              may use and occupy each year. You can check out our timeshare
              calendar to see examples of fixed week numbers.
            </li>
            <li>
              Floating timeshare ownership - A floating ownership is split into
              different seasons and usually given a colour or name for each
              season from which you may pick your desired timeshare holiday.
              Examples of seasons are known as: Platinum, Gold, Red, Blue.
            </li>
            <li>
              Points timeshare ownership - A points ownership gives you an
              allocation of annual points which are exchanged through a central
              reservation system to a desired resort held within a points
              portfolio of resorts. There are often extra areas of points
              ownership to include holiday extras, cruise holidays, cottage
              rental, car hire, etc.
            </li>
          </ol>
          <p className="pt-3 text-[#666]    font-extralight">
            We cover all destinations, resorts, and timeshare related products
            in the world. If you have a specific timeshare requirement and
            cannot find this. Please simply contact us and we will be happy to
            search for further availability.
          </p>
        </div>
        <div className="flex flex-wrap pt-8  justify-center">
          {clubs.map((val) => {
            return (
              <ClubCard
                key={val.name}
                name={val.name}
                link={val.link}
                image={val.image}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

const ClubCard = ({ image, name, link, travel }) => {
  return (
    <Link href={link} className={` lg:w-1/3  w-full p-2`}>
      <div className="flex justify-center pb-8 cursor-pointer w-full ">
        <div>
          <img
            src={image}
            className=" h-[220px] w-[270px] object-cover  hover:scale-105  duration-300 ease-in"
          />
          <div className="py-4 text-center text-[#886c3c] font-bold hover:text-main  text-lg max-w-[270px]">
            {name}
          </div>
          <p className=" text-[#666]   text-center  font-extralight">
            Available (Buy , Sell , Rent)
          </p>
        </div>
      </div>
    </Link>
  );
};
